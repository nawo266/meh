#!/bin/bash

#
#Checks if there is DMENU variable set, if not defaults to dmenu. If you want to use Rofi instead, set this variable to rofi -dmenu
#
[ -z "$DMENU" ] && DMENU=dmenu
echo "My dmenu is $DMENU"


#
#Screens settings function
#
setScreens () {

	monnum=$(xrandr -q | grep -w connected | awk '{print $1}' | wc -l)
	monitors=$(xrandr -q | grep -w connected | awk '{print $1}' |  sed 's/ \+/\n/g')
	chosenMonitor=$(xrandr -q | grep -w connected | awk '{print $1}' |  sed 's/ \+/\n/g' | $DMENU -p "Choose ur display")
	setChoice=$(echo -e "set right of\nset left of\nresolution\nset primary\non\noff" | $DMENU -p "What to do?")
	case "$setChoice" in
		"set right of") monitorLeft=$(xrandr -q | grep -w connected | awk '{print $1}' |  sed 's/ \+/\n/g' | $DMENU -p "Right of what?")
			xrandr --output $chosenMonitor --right-of $monitorLeft ;;
		"set left of")  monitorRight=$(xrandr -q | grep -w connected | awk '{print $1}' |  sed 's/ \+/\n/g' | $DMENU -p "left of what?")
			xrandr --output $chosenMonitor --left-of $monitorRight ;;
		"resolution") res=$(xrandr | awk -v monitor="^$chosenMonitor connected" '/connected/ {p = 0} $0 ~ monitor {p = 1} p' | grep -v "$chosenMonitor" | awk '{print $1}' | $DMENU)
			xrandr --output $chosenMonitor --mode $res ;;
		"set primary") xrandr --output $chosenMonitor --primary ;;
		"on") xrandr --output $chosenMonitor --auto ;;
		"off") xrandr --output $chosenMonitor --off ;;
	esac
}

choice=$(echo -e "Screens\nReset\n" | $DMENU -p "Much sEttings enHancer")
case "$choice" in
	"Screens") setScreens ;;
	"Reset") ;;
esac

